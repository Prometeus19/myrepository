//
//  ApartmentDescriptionViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/20/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ApartmentDescriptionViewController: UIViewController {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    
    var apartmentDescription = ""
    var price = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        decriptionLabel.text = apartmentDescription
        priceLabel.text = price
    }
}
