//
//  FinalPriceViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/14/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class FinalPriceViewController: UIViewController {

    lazy var myApartment = MyApartment.myApartment
    lazy var apartments = Apartments.apartments
    lazy var makeRequest = MakeRequest()
    lazy var searchTheSameApartment = SearchTheSameApartment()
    
    @IBOutlet weak var listOfApartmentsButton: InterfaceButton!
    @IBOutlet weak var waitingCountingLabel: UILabel!
    @IBOutlet weak var priceCalculationIndicator: UIActivityIndicatorView!
    @IBOutlet weak var finalPriceLabel: UILabel!
    
    var countArray = [Int]()
    var result = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listOfApartmentsButton.isHidden = true
        myApartment.buildDescription()
        makeRequest.getDataFromReques {
            let arrayAppartments = self.apartments.apartmentsArray
            self.result = self.searchTheSameApartment.searchTheSameApartment(in: arrayAppartments)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.priceCalculationIndicator.stopAnimating()
                self.priceCalculationIndicator.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                self.waitingCountingLabel.text = "Примерная стоимость:"
                self.finalPriceLabel.text = "\(self.result)$!"
                self.listOfApartmentsButton.isHidden = false
            })
        }
        priceCalculationIndicator.startAnimating()
        waitingCountingLabel.text = "Подождите пожалуйста, идут вычисления..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.alpha = 1
    }
}
