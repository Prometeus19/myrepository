//
//  ListOfApartmentsTableViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/20/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import RealmSwift

class ListOfApartmentsTableViewController: UITableViewController {
    
    var apartmentsArray: Results<ApartmentsList>?
    lazy var workWithDatabase = WorkWithDatabase()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        apartmentsArray = workWithDatabase.getData()
        guard apartmentsArray != nil else { return }
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let arr = apartmentsArray,
              arr.count != 0 else { return 0 }
        return arr.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApartmentCell", for: indexPath)
        guard let apArray = apartmentsArray else { return cell }
        let apartment = apArray[indexPath.row]
        cell.textLabel?.text = apartment.apartmentDescription
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let advc = storyboard?.instantiateViewController(withIdentifier: "DescriptionVC") as? ApartmentDescriptionViewController
        guard let apArray = apartmentsArray,
              let pushedVC = advc else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        pushedVC.apartmentDescription = apArray[indexPath.row].apartmentDescription
        pushedVC.price = "Цена: \(apArray[indexPath.row].apartmentPrice)"
        self.navigationController?.pushViewController(pushedVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
