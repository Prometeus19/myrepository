//
//  MainInformationViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/13/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class MainInformationApartmentViewController: UIViewController {
    
    lazy var myApartment = MyApartment.myApartment
    var recOfThingsVC: RecognizerOfThingsViewController?

    @IBOutlet weak var countRoomsLabel: UILabel!
    @IBOutlet weak var roomsSegmentedControl: UISegmentedControl!
    @IBOutlet weak var allFurnitureSwitch: UISwitch!
    @IBOutlet weak var wiFiSwitch: UISwitch!
    @IBOutlet weak var scanInformationButton: InterfaceButton!
    @IBOutlet weak var nextDoorTextView: UITextView!
    @IBOutlet weak var plumbingSwitch: UISwitch!
    @IBOutlet weak var repairsSwitch: UISwitch!
    @IBOutlet weak var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        scanInformationButton.setTitleOnButton("Отсканировать недостающее")
        guard myApartment.room == true else { return }
        roomsSegmentedControl.isEnabled = false
        countRoomsLabel.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.alpha = 1
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        myApartment.allFurniture = allFurnitureSwitch.isOn
        myApartment.countRooms = roomsSegmentedControl.selectedSegmentIndex + 1
        myApartment.plumbing = plumbingSwitch.isOn
        myApartment.whatIsOnTheNextDoor = nextDoorTextView.text
        myApartment.wiFi = wiFiSwitch.isOn
        myApartment.repairs = repairsSwitch.isOn
        if let address = addressTextField.text {
            myApartment.address = address
        }
    }
}
