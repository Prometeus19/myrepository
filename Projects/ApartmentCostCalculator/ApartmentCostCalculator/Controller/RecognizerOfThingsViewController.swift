//
//  ViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/10/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import AVKit
import Vision

class RecognizerOfThingsViewController: UIViewController,
                                        AVCaptureVideoDataOutputSampleBufferDelegate,
                                        ThingsListTableViewControllerDelegate {
    
    // MARK: Properties

    lazy var myApartment = MyApartment.myApartment
    lazy var constant = Constants.DrawButton()
    
    @IBOutlet weak var visionView: UIView!
    @IBOutlet weak var addNewThingsButton: InterfaceButton!
    @IBOutlet weak var thingsListButton: InterfaceButton!
    
    var thingsArray = [String]()
    
    // MARK: Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        thingsListButton.setButtonSize()
        thingsListButton.doButtonBorder()
        
        let captureSession =  AVCaptureSession()
        guard let captureDevice = AVCaptureDevice.default(for: .video),
              let input = try? AVCaptureDeviceInput(device: captureDevice) else { return }
        captureSession.addInput(input)
        captureSession.startRunning()
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = visionView.bounds
        previewLayer.zPosition = 0.5
        visionView.layer.addSublayer(previewLayer)
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        dataOutput.alwaysDiscardsLateVideoFrames = true
        captureSession.addOutput(dataOutput)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.alpha = 0
        thingsListButton.setTitleOnButton("\(thingsArray.count)")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
              identifier == "Show Things",
              let tltvc = segue.destination as? ThingsListTableViewController else { return }
        tltvc.delegate = self
        tltvc.thingsArray = thingsArray
    }
    
    // MARK: Actions functions
    @IBAction func addNewThingsInDescription(_ sender: UIButton) {
        guard let resultString = sender.titleLabel?.text?.lowercased() else { return }
        switch resultString {
        case "холодильник":       setPropertiesMyApartment(&myApartment.fridge, resultString)
        case "свч":               setPropertiesMyApartment(&myApartment.microwaveOven, resultString)
        case "телевизор":         setPropertiesMyApartment(&myApartment.television, resultString)
        case "стиральная машина": setPropertiesMyApartment(&myApartment.washer, resultString)
        default:                  break
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer),
            let model = try? VNCoreMLModel(for: MySecondModel().model) else { return }
        
        let request = VNCoreMLRequest(model: model) { (finishedRequest, _) in
            guard let results = finishedRequest.results as? [VNClassificationObservation],
                let firstObservation = results.first else { return }
            
            DispatchQueue.main.async {
                switch firstObservation.identifier {
                case "Fridge":         self.addNewThingsButton.setTitleOnButton("Холодильник")
                case "Microwave oven": self.addNewThingsButton.setTitleOnButton("СВЧ")
                case "Washer":         self.addNewThingsButton.setTitleOnButton("Стиральная машина")
                case "Television":     self.addNewThingsButton.setTitleOnButton("Телевизор")
                default:               self.addNewThingsButton.setTitleOnButton("try again!")
                }
            }
        }
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
    }
    
    func setPropertiesMyApartment(_ myAppartmentPropertie: inout String,
                                  _ resultString: String) {
        myAppartmentPropertie = resultString
        guard thingsArray.contains(resultString) else {
            thingsListButton.doAnimationForButton()
            thingsArray.append(resultString)
            thingsListButton.setTitleOnButton("\(thingsArray.count)")
            return
        }
    }
    
    func removeItemsFromThingArray(at index: Int) {
        thingsArray.remove(at: index)
    }
}
