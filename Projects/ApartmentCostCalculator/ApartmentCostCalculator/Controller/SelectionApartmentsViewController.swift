//
//  SelectionApartmentsViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/14/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class SelectionApartmentsViewController: UIViewController {
    
    lazy var myApartment = MyApartment.myApartment
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.alpha = 1
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        guard identifier == "Flat" else {
            myApartment.room = true
            myApartment.flat = false
            return
        }
        myApartment.flat = true
        myApartment.room = false
    }
}
