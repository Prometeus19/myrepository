//
//  ThingsListTableViewController.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/14/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

protocol ThingsListTableViewControllerDelegate: class {
    func removeItemsFromThingArray(at index: Int)
}

class ThingsListTableViewController: UITableViewController {
    
    lazy var myApartment = MyApartment.myApartment
    weak var delegate: ThingsListTableViewControllerDelegate?
    
    var thingsArray = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.alpha = 1
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return thingsArray.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OneThing", for: indexPath)
        let thing = thingsArray[indexPath.row]
        cell.textLabel?.text = thing
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCell.EditingStyle,
                            forRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath),
              let cellText = cell.textLabel?.text,
              editingStyle == .delete else { return }
        thingsArray.remove(at: indexPath.row)
        delegate?.removeItemsFromThingArray(at: indexPath.row)
        switch cellText {
        case "холодильник":       myApartment.fridge = ""
        case "свч":               myApartment.microwaveOven = ""
        case "телевизор":         myApartment.television = ""
        case "стиральная машина": myApartment.washer = ""
        default:                  break
        }
        tableView.reloadData()
    }
}
