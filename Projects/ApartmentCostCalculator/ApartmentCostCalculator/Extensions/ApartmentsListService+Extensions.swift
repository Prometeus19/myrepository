//
//  ApartmentsListService+Extensions.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/12/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Moya

extension ApartmentsListService: TargetType {
    
    var baseURL: URL {
        return URL(string: "http://feeds.feedburner.com/kvartirantby")!
    }
    
    var path: String {
        switch self {
        case .getApartmentsList:
            return ""
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String: String]? {
        return ["text/xml": "Content-Type"]
    }
}
