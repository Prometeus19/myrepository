//
//  Constants.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/13/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

struct Constants {
    
    struct Random {
        let lowerBoundsOfRandomInterval = 250
        let upperBoundsOfRandomInterval = 600
    }
    
    struct DrawButton {
        let buttonCornerRadius: CGFloat = 15
    }
}
