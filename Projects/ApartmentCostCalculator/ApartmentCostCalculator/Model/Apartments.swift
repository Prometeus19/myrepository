//
//  Apartments.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/14/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

class Apartments {
    
    static let apartments = Apartments()
    
    private init() {}
    
    var apartmentsArray = [Apartment]()
}
