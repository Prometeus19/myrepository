//
//  ApartmentsList.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/20/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import RealmSwift

class ApartmentsList: Object {
    
    @objc dynamic var apartmentDescription = ""
    @objc dynamic var apartmentPrice = 0
}
