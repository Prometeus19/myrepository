//
//  ChannelItems.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/12/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import EVReflection

class ChannelItems: EVObject {
    
    var item: [DescriptionItems]?
    
    override func setValue(_ value: Any, forUndefinedKey key: String) {
        switch key {
        case "item": item = value as? [DescriptionItems]
        default:     break
        }
    }
}
