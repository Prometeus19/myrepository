//
//  DescriptionItems.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/12/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import EVReflection

class DescriptionItems: EVObject {
    
    var descriptionItem: String?
    
    var price = 0
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        switch key {
        case "description":
            descriptionItem = value as? String
            let constant = Constants.Random()
            price = Int.random(in: constant.lowerBoundsOfRandomInterval...constant.upperBoundsOfRandomInterval)
        default:
            break
        }
    }
}
