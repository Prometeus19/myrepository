//
//  ListOfInformationAboutApartments.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/12/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import EVReflection

class ListOfInformationAboutApartments: EVObject {
    
    var channel: ChannelItems?
    var badWordsArray = ["Продается", "Снимет", "Сниму", "продается", "снимет", "сниму", "Продам", "агент", "Дом", "дом", " агент"]
    
    override func setValue(_ value: Any?, forKey key: String) {
        switch key {
        case "channel":
            guard let getChannel = value as? ChannelItems else { return }
            channel = filterChannel(getChannel)
        default:
            break
        }
    }
    
    func filterChannel(_ getChannel: ChannelItems) -> ChannelItems {
        guard var channelItemsArray = getChannel.item else { return getChannel }
        for index in 0..<channelItemsArray.count where index < channelItemsArray.count {
            guard let description = channelItemsArray[index].descriptionItem else { return getChannel }
            searchBadWords(in: description, &channelItemsArray)
        }
        getChannel.item = channelItemsArray
        return getChannel
    }
    
    func searchBadWords(in description: String, _ channelItemsArray: inout [DescriptionItems]) {
        for (wordIndex, word) in badWordsArray.enumerated() {
            guard description.contains(word) else { continue }
            channelItemsArray.remove(at: wordIndex)
            break
        }
    }
}
