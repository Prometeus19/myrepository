//
//  MyApartment.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/13/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class MyApartment {
    
    static let myApartment = MyApartment()
    
    private init() {}
    
    var room = false
    var flat = true
    
    var price = 0
    var countRooms: Int?
    var wiFi = false
    var allFurniture = false
    var plumbing = false
    var repairs = false
    var description = ""
    var whatIsOnTheNextDoor = ""
    var address = ""
    
    var fridge = ""
    var microwaveOven = ""
    var television = ""
    var washer = ""
    
    func buildDescription() {
        guard room == true else {
            description = "Сдается комната по " + address + ". " + "Рядом " + whatIsOnTheNextDoor + "."
            fillDescription()
            return
        }
        description = "Сдается " + String(describing: countRooms) +
            "-комнатная квартира по " + address + ". " + "Рядом " + whatIsOnTheNextDoor + "."
        fillDescription()
    }
    
    func fillDescription() {
        if allFurniture == true {
            description += "Есть вся мебель, " + fridge + " " + microwaveOven + " " +
                television + " " + washer + ", "
        } else {
            description += "В квартире присутсвуют " + fridge + " " + microwaveOven +
                " " + television + " " + washer + ", "
        }
        if wiFi == true {
            description += "Wi-Fi"
        }
        if repairs == true {
            description += " , качественный ремонт."
        }
    }
}
