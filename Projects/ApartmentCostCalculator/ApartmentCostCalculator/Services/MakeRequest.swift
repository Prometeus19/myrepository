//
//  MakeRequest.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/11/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import Moya
import EVReflection

enum ApartmentsListService {
    case getApartmentsList
}

class MakeRequest {
    
    lazy var provider = MoyaProvider<ApartmentsListService>()
    lazy var json = WorkWithJSON()
    
    func getDataFromReques(completionHandler: @escaping () -> Void) {
        provider.request(.getApartmentsList) { (result) in
            switch result {
            case .success(let response):
                do {
                    let parsed = try response.RmapXml(to: ListOfInformationAboutApartments.self)
                    self.json.saveDataInJson(parsed)
                    self.json.readDataFromJson()
                    completionHandler()
                } catch {
                    self.json.readDataFromJson()
                    completionHandler()
                }
            case .failure:
                self.json.readDataFromJson()
                completionHandler()
            }
        }
    }
}
