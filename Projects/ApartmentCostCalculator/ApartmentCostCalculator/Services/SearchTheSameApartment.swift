//
//  SearchTheSameApartment.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/14/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

class SearchTheSameApartment {
    
    lazy var myApartment = MyApartment.myApartment
    lazy var apartments = Apartments.apartments
    
    var countArray = [Int]()
    
    func searchTheSameApartment(in arrayApartments: [Apartment]) -> Int {
        let setFromMyApartmentDescription = Set(myApartment.description.split(separator: " "))
        for apartment in arrayApartments {
            let setFromApartmentDescription = Set(apartment.apartmentDescription.split(separator: " "))
            let resultArray = Array(setFromApartmentDescription.intersection(setFromMyApartmentDescription))
            countArray.append(resultArray.count)
        }
        guard let maxValue = countArray.max() else { return 0 }
        let index = searchIndexMaxValue(in: countArray, maxValue)
        return arrayApartments[index].price
    }
    
    func searchIndexMaxValue(in array: [Int], _ maxValue: Int) -> Int {
        for index in 0..<array.count {
            guard array[index] == maxValue else { continue }
            return index
        }
        return 0
    }
}
