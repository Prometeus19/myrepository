//
//  WorkWithDatabase.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/21/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import RealmSwift

class WorkWithDatabase {
    
    lazy var apartments = Apartments.apartments
    var apartmentsArray: Results<ApartmentsList>?
    let realm = try? Realm()
    
    func getData() -> Results<ApartmentsList>? {
        guard let realmIsEmpty = realm?.isEmpty else { return nil }
        
        guard realmIsEmpty else {
            apartmentsArray = realm?.objects(ApartmentsList.self)
            return apartmentsArray
        }
        
        writeDataInDatabase()    
        apartmentsArray = realm?.objects(ApartmentsList.self)
        return apartmentsArray
    }
    
    func writeDataInDatabase() {
        for apartment in apartments.apartmentsArray {
            let apartmentsList = ApartmentsList()
            apartmentsList.apartmentDescription = apartment.apartmentDescription
            apartmentsList.apartmentPrice = apartment.price
            try? realm?.write { realm?.add(apartmentsList) }
        }
    }
}
