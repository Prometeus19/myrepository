//
//  WorkWithJSON.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/13/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class WorkWithJSON {
    
    lazy var apartments = Apartments.apartments
    
    var jsonData: Data?
    var url: URL?
    let fileManager = FileManager.default
    
    func saveDataInJson(_ parsed: ListOfInformationAboutApartments) {
        guard let parsedArray = parsed.channel?.item else { return }
        var dataArray = [[String: Any]]()
        for inform in parsedArray {
            var informDict = [String: Any]()
            informDict["price"] = inform.price
            informDict["description"] = inform.descriptionItem
            dataArray.append(informDict)
        }
        
        jsonData = try? JSONSerialization.data(withJSONObject: dataArray, options: .prettyPrinted)
        url = try? fileManager.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        guard let jsonUrl = url?.appendingPathComponent("apartments_information.json") else { return }
        try? jsonData?.write(to: jsonUrl)
    }
    
    func readDataFromJson() {
        url = try? fileManager.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        guard let jsonUrl = url?.appendingPathComponent("apartments_information.json") else { return }
        jsonData = try? Data(contentsOf: jsonUrl)
        
        guard let jsonReadData = jsonData else { return }
        let parsedInform = try? JSONSerialization.jsonObject(with: jsonReadData, options: .mutableContainers)
        guard let inform = parsedInform as? [[String: Any]] else { return }
        for data in inform {
            guard let apartmentPrice = data["price"] as? Int,
                  let apartmentDescription = data["description"] as? String else { continue }
            let apartment = Apartment()
            apartment.price = apartmentPrice
            apartment.apartmentDescription = apartmentDescription
            apartments.apartmentsArray.append(apartment)
        }
    }
}
