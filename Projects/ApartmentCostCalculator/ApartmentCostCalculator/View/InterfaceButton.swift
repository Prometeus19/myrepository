//
//  AddButton.swift
//  ApartmentCostCalculator
//
//  Created by Prometei on 6/13/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class InterfaceButton: UIButton {
    
    lazy var constant = Constants.DrawButton()

    override func draw(_ rect: CGRect) {
        self.layer.zPosition = 1
        self.layer.cornerRadius = constant.buttonCornerRadius
        self.clipsToBounds = true
    }
    
    func setTitleOnButton(_ title: String) {
        self.setTitle(title, for: .normal)
    }
    
    func doButtonBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func doAnimationForButton() {
        UIView.animate(withDuration: 1,
                       delay: 0,
                       options: [],
                       animations: {
                        self.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        },
                       completion: { _ in
                        self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setButtonSize() {
        self.frame.size = CGSize(width: constant.buttonCornerRadius * 2,
                                 height: constant.buttonCornerRadius * 2)
    }
}
