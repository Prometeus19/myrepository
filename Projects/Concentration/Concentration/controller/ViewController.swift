//
//  ViewController.swift
//  Concentration
//
//  Created by Prometei on 4/19/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    lazy var theme = Themes()

    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!

    var emojiChoises: [String] = []
    var emoji: [Int: String] = [:]
    
    override func viewDidLoad() {
        emojiChoises = theme.chooseRandomTheme()
    }

    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        game.chooseCard(at: cardNumber)
        updateLabels("Flips: \(game.countFlipCard)", "Score: \(game.countScore)")
        updateViewFromModel()
    }
    
    @IBAction func startNewGame(_ sender: UIButton) {
        emoji.removeAll()
        emojiChoises.removeAll()
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        updateLabels("Flips: 0", "Score: 0")
        viewDidLoad()
        updateViewFromModel()
    }

    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            }
        }
    }
    
    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoises.count > 0 {
            let randomIndex = Int.random(in: 1..<emojiChoises.count)
            emoji[card.identifier] = emojiChoises.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
    
    func updateLabels(_ firstTitleLabel: String, _ secondTitleLabel: String) {
        flipCountLabel.text = firstTitleLabel
        scoreLabel.text = secondTitleLabel
    }
}

