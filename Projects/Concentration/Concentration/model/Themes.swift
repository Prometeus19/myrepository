//
//  Themes.swift
//  Concentration
//
//  Created by Prometei on 4/22/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

struct Themes {
    var themesArray = [["⚽️", "🥎", "🏈", "🏐", "⚾️", "🏓", "🏀", "🥊", "🎳"],
                       ["😃", "😝", "🙃", "😎", "🤪", "🤩", "☺️", "😉", "🤫"],
                       ["🐶", "🐱", "🦁", "🐯", "🐨", "🐼", "🐰", "🐻", "🐸"],
                       ["🍏", "🍎", "🍇", "🥩", "🍕", "🍌", "🍩", "🍔", "🌶"],
                       ["🏋🏼‍♀️", "🤼‍♀️", "⛹🏼‍♂️", "🤸🏼‍♂️", "🏄🏿‍♂️", "🏊🏼‍♀️", "🚴🏼‍♂️", "🚣🏼‍♂️", "🏇🏼"],
                       ["♞", "♝", "♠︎", "☽", "☾", "♙", "♕", "♜", "☯︎"]]
    
    mutating func chooseRandomTheme() -> [String] {
        let randomIndex = Int.random(in: 1..<themesArray.count)
        return themesArray[randomIndex]
    }
}
