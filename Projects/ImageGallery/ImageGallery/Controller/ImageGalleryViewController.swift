//
//  ImageGalleryViewController.swift
//  ImageGallery
//
//  Created by Prometei on 5/29/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import SDWebImage

class ImageGalleryViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var imageCollectionView: UICollectionView! {
        didSet {
            imageCollectionView.addInteraction(UIDropInteraction(delegate: self))
            imageCollectionView.dataSource = self
            imageCollectionView.delegate = self
            imageCollectionView.dragDelegate = self
            imageCollectionView.dropDelegate = self
            imageCollectionView.dragInteractionEnabled = true
        }
    }
    
    lazy var constant = Constants()
    lazy var cache = SaveImageInCache()
    
    var document: ImageGalleryDocument?
    var imageArray = [UIImage]()
    var urlArray = [URL]()
    var sizeImage: UIImage?
    
    var imageGallery: ImageGallery? {
        get {
            return ImageGallery(urls: urlArray)
        }
        set {
            imageArray.removeAll()
            urlArray.removeAll()
            
            guard let urls = newValue?.urls else { return }
            for imageUrl in urls {
                let imgFromMemory = cache.extractImageFromCache(for: imageUrl)
                guard let newImageFromCach = imgFromMemory else {
                    cache.downloadImage(imageUrl: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.cache.saveImage(url: imageUrl, toCache: image, complation: {})
                            self.addImagesInCollection(image, imageUrl)
                        }
                    }
                    continue
                }
                addImagesInCollection(newImageFromCach, imageUrl)
            }
        }
    }
    
    // MARK: Override functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        document?.open(completionHandler: { (success) in
            if success {
                self.title = self.document?.localizedName
                self.imageGallery = self.document?.imageGallary
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
              identifier == "Show Image",
              let imageCell = sender as? ImageCollectionViewCell,
              let indexPath = imageCollectionView.indexPath(for: imageCell),
              let destinationController = segue.destination as? ScrollImageViewController else { return }
        destinationController.imageURL = urlArray[indexPath.item]
    }
    
    // MARK: All functions, that work with collection
    
    func addImagesInCollection(_ image: UIImage, _ url: URL) {
        urlArray.append(url)
        imageArray.append(image)
        imageCollectionView.reloadData()
    }
  
    func changePlaceItemsOnBoard(with coordinator: UICollectionViewDropCoordinator,
                                 at sourceIndexPath: IndexPath,
                                 at destinationIndexPath: IndexPath,
                                 _ item: UICollectionViewDropItem) {
        guard let image = item.dragItem.localObject as? UIImage else { return }
        imageCollectionView.performBatchUpdates({
            imageArray.remove(at: sourceIndexPath.item)
            let url = urlArray.remove(at: sourceIndexPath.item)
            imageArray.insert(image, at: destinationIndexPath.item)
            urlArray.insert(url, at: destinationIndexPath.item)
            imageCollectionView.deleteItems(at: [sourceIndexPath])
            imageCollectionView.insertItems(at: [destinationIndexPath])
        })
        coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
    }
    
    func downloadImages(with coordinator: UICollectionViewDropCoordinator,
                        _ item: UICollectionViewDropItem,
                        at destinationIndexPath: IndexPath) {
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self, completionHandler: { (provider, _) in
            DispatchQueue.main.async {
                guard let url = provider as? URL else { return }
                self.urlArray.insert(url, at: destinationIndexPath.item)
                self.imageCollectionView.reloadData()
            }
        })
        
        item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { (provider, _) in
            DispatchQueue.main.async {
                guard let image = provider as? UIImage else { return }
                self.imageArray.insert(image, at: destinationIndexPath.item)
                self.saveGallery()
                self.imageCollectionView.reloadItems(at: self.imageCollectionView.indexPathsForVisibleItems)
            }
        }
    }

    func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard let image = (imageCollectionView.cellForItem(at: indexPath) as?
            ImageCollectionViewCell)?.backImageView.image else { return [] }
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = image
        return [dragItem]
    }

    func saveGallery() {
        document?.imageGallary = imageGallery
        if document?.imageGallary != nil {
            document?.updateChangeCount(.done)
        }
    }
    
    // MARK: Actions functions
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        saveGallery()
        
        guard document?.imageGallary != nil else { return }
        document?.thumbnail = imageCollectionView.snapshot
        dismiss(animated: true) {
            self.document?.close()
        }
    }
}

extension ImageGalleryViewController: UICollectionViewDropDelegate,
                                      UIDropInteractionDelegate {
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == imageCollectionView
        if isSelf {
            return session.canLoadObjects(ofClass: UIImage.self)
        } else {
            return session.canLoadObjects(ofClass: NSURL.self) &&
                session.canLoadObjects(ofClass: UIImage.self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        
        for item in coordinator.items {
            guard let sourceIndexPath = item.sourceIndexPath else {
                downloadImages(with: coordinator,
                               item,
                               at: destinationIndexPath)
                return
            }
            changePlaceItemsOnBoard(with: coordinator,
                                    at: sourceIndexPath,
                                    at: destinationIndexPath,
                                    item)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == imageCollectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy,
                                            intent: .insertAtDestinationIndexPath)
    }
}

extension ImageGalleryViewController: UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        itemsForBeginning session: UIDragSession,
                        at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = imageCollectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        itemsForAdditingTo session: UIDragSession,
                        at indexPath: IndexPath) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
}

extension ImageGalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard imageArray.count == urlArray.count else { return CGSize.zero }
        sizeImage = imageArray[indexPath.item].resize(maxWidthHeight: Double((collectionView.bounds.width / 2) -
            constant.spaceBetweenItems))
        guard let returnImage = sizeImage else { return CGSize.zero }
        return returnImage.size
    }
}

extension ImageGalleryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        
        guard let imageCell = cell as? ImageCollectionViewCell else { return cell }
        imageCell.downoladImageActivityIndicetor.startAnimating()
        guard imageArray.count == urlArray.count else { return cell }
        imageCell.backImageView.image = imageArray[indexPath.item]
        imageCell.doIndicatorClean()
        
        return cell
    }
}
