//
//  ScrollImageViewController.swift
//  ImageGallery
//
//  Created by Prometei on 6/5/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

// MARK: File from lecture number 10, veeeery professional integration ;)

import UIKit

class ScrollImageViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 0.03
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var imageView  = UIImageView()
    var autoZoomed = true
    
    var imageURL: URL? {
        didSet {
            image = nil
            if view.window != nil {
                fetchImage()
            }
        }
    }
    
    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size
            spinner?.stopAnimating()
            autoZoomed = true
            zoomScaleToFit()
        }
    }
    
    // MARK: Override functions
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if imageView.image == nil { fetchImage() }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        zoomScaleToFit()
    }
    
    // MARK: Functions of working with UIScrollView
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        autoZoomed = false
    }
    
    func fetchImage() {
        autoZoomed = true
        guard let url = imageURL else { return }
        spinner.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let urlContents = try? Data(contentsOf: url)
            DispatchQueue.main.async {
                guard let imageData = urlContents,
                      url == self?.imageURL else { return }
                self?.image = UIImage(data: imageData)
            }
        }
    }
    
    func zoomScaleToFit() {
        if !autoZoomed { return }
        guard let imageScrollView = scrollView else { return }
        if image != nil &&
           (imageView.bounds.size.width > 0) &&
           (scrollView.bounds.size.width > 0) {
            let widthRatio = scrollView.bounds.size.width  / imageView.bounds.size.width
            let heightRatio = scrollView.bounds.size.height / self.imageView.bounds.size.height
            imageScrollView.zoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio
            imageScrollView.contentOffset = CGPoint(x: (imageView.frame.size.width -
                                                        imageScrollView.frame.size.width) / 2,
                                                    y: (imageView.frame.size.height -
                                                        imageScrollView.frame.size.height) / 2)
        }
    }
}
