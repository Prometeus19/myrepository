//
//  UIImage+Extensions.swift
//  ImageGallery
//
//  Created by Prometei on 5/29/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension UIImage {
    
    func resize(maxWidthHeight: Double) -> UIImage? {
        let hasAlpha = true
        let scale: CGFloat = 0.0
        let actualHeight = Double(size.height)
        let actualWidth = Double(size.width)
        var maxWidth = 0.0
        var maxHeight = 0.0
        
        if actualWidth > actualHeight {
            maxWidth = maxWidthHeight
            let per = maxWidthHeight / actualWidth
            maxHeight = actualHeight * per
        } else {
            let per = actualHeight / actualWidth
            maxHeight = maxWidthHeight * per
            maxWidth = maxWidthHeight
        }
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: maxWidth,
                                                      height: maxHeight),
                                               !hasAlpha,
                                               scale)
        self.draw(in: CGRect(origin: .zero, size: CGSize(width: maxWidth,
                                                         height: maxHeight)))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
}
