//
//  UIViewController+Extensions.swift
//  ImageGallery
//
//  Created by Prometei on 6/3/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var contents: UIViewController {
        guard let navcon = self as? UINavigationController else { return self }
        return navcon.visibleViewController ?? navcon
    }
}
