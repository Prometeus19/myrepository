//
//  Constants.swift
//  ImageGallery
//
//  Created by Prometei on 5/31/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

struct Constants {
    let spaceBetweenItems: CGFloat = 5
}
