//
//  ImageGallary.swift
//  ImageGallery
//
//  Created by Prometei on 6/3/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

struct ImageGallery: Codable {
    
    var urls = [URL]()
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(urls: [URL]) {
        self.urls = urls
    }
    
    init?(json: Data) {
        guard let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) else { return nil }
        self = newValue
    }
}
