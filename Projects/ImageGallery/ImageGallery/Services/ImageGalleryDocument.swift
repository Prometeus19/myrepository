//
//  Document.swift
//  ImageGallery
//
//  Created by Prometei on 6/3/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ImageGalleryDocument: UIDocument {
    
    var imageGallary: ImageGallery?
    var thumbnail: UIImage?
    
    override func contents(forType typeName: String) throws -> Any {
        return imageGallary?.json ?? Data()
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        guard let json = contents as? Data else { return }
        imageGallary = ImageGallery(json: json)
    }
    
    override func fileAttributesToWrite(to url: URL,
                                        for saveOperation: UIDocument.SaveOperation) throws -> [AnyHashable: Any] {
        var attributes = try super.fileAttributesToWrite(to: url, for: saveOperation)
        guard let thumbnail = self.thumbnail else { return attributes }
        attributes[URLResourceKey.thumbnailDictionaryKey] =
            [URLThumbnailDictionaryItem.NSThumbnail1024x1024SizeKey: thumbnail]
        return attributes
    }
}
