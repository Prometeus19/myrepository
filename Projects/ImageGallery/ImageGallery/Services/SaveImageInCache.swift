//
//  DownloadImages.swift
//  ImageGallery
//
//  Created by Prometei on 6/5/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import SDWebImage

struct SaveImageInCache {
    
    func downloadImage(imageUrl: URL, completionHandler: @escaping (UIImage) -> Void) {
        SDWebImageDownloader.shared.downloadImage(with: imageUrl) { (image, _, _, _) in
            guard let newImage = image else { return }
            completionHandler(newImage)
        }
    }
    
    func saveImage(url: URL, toCache: UIImage?, complation: @escaping SDWebImageNoParamsBlock) {
        guard let cache = toCache else { return }
        let manager = SDWebImageManager.shared
        let data = cache.pngData()
        guard let key = manager.cacheKey(for: url) else { return }
        manager.imageCache.store(toCache,
                                 imageData: data,
                                 forKey: key,
                                 cacheType: SDImageCacheType.all,
                                 completion: complation)
    }
    
    func extractImageFromCache(for url: URL) -> UIImage? {
        let manager = SDWebImageManager.shared
        guard let key = manager.cacheKey(for: url),
              let image = SDImageCache.shared.imageFromCache(forKey: key) else { return nil }
        return image
    }
}
