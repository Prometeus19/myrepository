//
//  ImageCollectionViewCell.swift
//  ImageGallery
//
//  Created by Prometei on 5/29/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var downoladImageActivityIndicetor: UIActivityIndicatorView!
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 20
        backImageView.layer.cornerRadius = 20
        backImageView.clipsToBounds = true
    }
    
    func doIndicatorClean() {
        downoladImageActivityIndicetor.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    }
}
