//
//  CGPoint+Extensions.swift
//  Set
//
//  Created by Prometei on 5/15/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension CGPoint {
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint(x: x + dx, y: y + dy)
    }
}
