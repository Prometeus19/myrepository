//
//  BoardView.swift
//  Set
//
//  Created by Prometei on 5/11/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class BoardView: UIView {
    
    lazy var constantForBoard = Constants.Board()
    
    var cardViews = [CardView]() {
        willSet { removeSubviews() }
        didSet { addSubviews() }
    }
    
    func removeSubviews() {
        for card in cardViews {
            card.removeFromSuperview()
        }
    }
    
    func addSubviews() {
        for card in cardViews {
            addSubview(card)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var grid = Grid(layout: Grid.Layout.aspectRatio(constantForBoard.cellAspectRatio),
                        frame: bounds)
        grid.cellCount = cardViews.count
        for row in 0..<grid.dimensions.rowCount {
            for column in 0..<grid.dimensions.columnCount {
                if cardViews.count > (row * grid.dimensions.columnCount + column) {
                    guard let gridCards = grid[row, column] else { return }
                    cardViews[row * grid.dimensions.columnCount + column].frame = gridCards.insetBy(
                        dx: constantForBoard.spacingDx, dy: constantForBoard.spacingDy)
                }
            }
        }
    }
}
