//
//  ViewController.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Set()
    
    @IBOutlet weak var boardView: BoardView! {
        didSet {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dealThreeCards(_:)))
            swipeDown.direction = .down
            boardView.addGestureRecognizer(swipeDown)
            
            let rotate = UIRotationGestureRecognizer(target: self, action: #selector(shuffleAllCardsOnTable))
            boardView.addGestureRecognizer(rotate)
        }
    }
    
    @IBOutlet weak var setLabel: UILabel!
    @IBOutlet weak var dealThreeCardsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabel.text = ""
        game.createArrayCardsForGame()
        updateViewFromModel()
    }
    
    @IBAction func startNewGame(_ sender: UIButton) {
        Card.identifierFactory = 0
        game = Set()
        boardView.cardViews.removeAll()
        dealThreeCardsButton.isEnabled = true
        viewDidLoad()
    }
    
    @IBAction func dealThreeCards(_ sender: UIButton) {
        if game.cards.count > 0 {
            game.dealThreeCards()
            updateViewFromModel()
        } else {
            dealThreeCardsButton.isEnabled = false
        }
    }
    
    @IBAction func searchSetOnTable(_ sender: UIButton) {
        if !game.searchSetOnTable() && game.cards.count == 0 {
            showAlert("End of the game!", "There are no more sets on the table! YOU WON!:) Start new game!")
        } else if !game.searchSetOnTable() && game.cards.count > 0 {
            showAlert("No sets!", "There are no sets on the table! Deal three more cards!")
        } else if game.searchSetOnTable() {
            updateViewFromModel()
        }
    }
    
    func updateViewFromModel() {
        updateCardViewsFromModel()
        switch game.stateCards {
        case .set:      setLabel.text = "SET!"
        case .notSet:   setLabel.text = "NOT SET!"
        case .nothing:  break
        }
    }
    
    func updateCardViewsFromModel() {
        if boardView.cardViews.count - game.cardsInGame.count > 0 {
            let cardViews = boardView.cardViews[..<game.cardsInGame.count]
            boardView.cardViews = Array(cardViews)
        }
        let numberCardViews = boardView.cardViews.count
        
        for index in game.cardsInGame.indices {
            let card = game.cardsInGame[index]
            if index > (numberCardViews - 1) {
                let cardView = CardView()
                updateCardView(cardView, for: card)
                addTapGestureRecognizer(for: cardView)
                boardView.cardViews.append(cardView)
            } else {
                let cardView = boardView.cardViews[index]
                updateCardView(cardView, for: card)
            }
        }
    }
    
    func updateCardView(_ cardView: CardView, for card: Card) {
        guard let figure = card.figure, let filling = card.filling else { return }
        cardView.figure = figure
        cardView.filling = filling
        cardView.color = card.color
        cardView.numberOfFigures = card.numberOfFigures
        cardView.isSelected = card.isSelected
    }
    
    func addTapGestureRecognizer(for cardView: CardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCard(recognizedBy: )))
        cardView.addGestureRecognizer(tap)
    }
    
    @objc func tapOnCard(recognizedBy recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            guard let cardView = recognizer.view as? CardView else { return }
            guard let index = boardView.cardViews.firstIndex(of: cardView) else { return }
            game.chooseCard(at: index)
        default:
            break
        }
        updateViewFromModel()
    }
    
    func showAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func shuffleAllCardsOnTable() {
        game.shuffleCardsInGame()
        updateViewFromModel()
    }
}

