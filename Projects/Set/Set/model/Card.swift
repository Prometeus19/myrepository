//
//  Card.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

enum Figures {
    case squiggle, diamond, oval
}

enum Filling {
    case solid, striped, unfilled
}

struct Card {
    
    var figure: Figures?
    var numberOfFigures = 0
    var color = UIColor.white
    var filling: Filling?
    
    var isSelected = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        Card.identifierFactory += 1
        return Card.identifierFactory
    }
}
