//
//  CGFloat+Extensions.swift
//  SetAndConcentration
//
//  Created by Prometei on 5/22/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension CGFloat {
    var random: CGFloat {
        return self * CGFloat(UInt32.random(in: .min ... .max) / UInt32.max)
    }
}
