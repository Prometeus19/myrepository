//
//  CGPoint+Extensions.swift
//  Set
//
//  Created by Prometei on 5/15/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension CGPoint {
    func offsetBy(sqdx: CGFloat, sqdy: CGFloat) -> CGPoint {
        return CGPoint(x: x + sqdx, y: y + sqdy)
    }
}
