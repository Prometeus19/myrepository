//
//  CGRect+Extensions.swift
//  Set
//
//  Created by Prometei on 5/15/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

extension CGRect {
    func zoomed(by scale: CGFloat) -> CGRect {
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width - newWidth) / 2, dy: (height - newHeight) / 2)
    }
}
