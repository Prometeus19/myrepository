//
//  ConstantValuesOfInterval.swift
//  Set
//
//  Created by Prometei on 5/2/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

struct Constants {
    
    struct Deck {
        let allColorRange = 27
        let endIndexOutlineRange = 9
        let endIntexStripedRange = 18
        let endIndexOfFilledRange = 27
        let maxNumberOfFigures = 3
    }
    
    struct Board {
        let cellAspectRatio: CGFloat = 0.625
        let spacingDx: CGFloat = 3.0
        let spacingDy: CGFloat = 3.0
    }
    
    struct Draw {
        let maxFaceSizeToBoundsSize: CGFloat = 0.75
        let pipHeightToFaceHeight: CGFloat = 0.25
        let cornerRadiusToBoundsHeight: CGFloat = 0.06
        let faceFrame: CGFloat = 0.60
        let interStripeSpace: CGFloat = 5.0
        let borderWidth: CGFloat = 3.0
        let spaceBetweenButtons = 54
    }
    
    struct Animation {
        let startingControllerTimeInterval = 0.005
    }
}
