//
//  BoardView.swift
//  Set
//
//  Created by Prometei on 5/11/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class BoardView: UIView {
    
    lazy var constantForBoard = Constants.Board()
    
    var cardViews = [CardView]()
    var rowsGrid: Int { return gridCards?.dimensions.rowCount ?? 0 }
    var gridCards: Grid?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gridCards = Grid(layout: Grid.Layout.aspectRatio(constantForBoard.cellAspectRatio),
                         frame: bounds)
        gridCards?.cellCount = cardViews.count
        layoutSetCards()
    }
    
    func layoutSetCards() {
        guard let grid = gridCards else { return }
        let columnsGrid = grid.dimensions.columnCount
        for row in 0..<rowsGrid {
            for column in 0..<columnsGrid where
                cardViews.count > (row * columnsGrid + column) {
                    doAnimationForBoard(row, column, columnsGrid, in: grid)
            }
        }
    }
    
    func doAnimationForBoard(_ row: Int, _ column: Int, _ columnsGrid: Int, in grid: Grid) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.3,
            delay: TimeInterval(row) * 0.1,
            options: [.curveEaseInOut],
            animations: {
                self.cardViews[row * columnsGrid + column].frame =
                    grid[row, column]!.insetBy(dx: self.constantForBoard.spacingDx,
                                               dy: self.constantForBoard.spacingDy)
        })
    }
    
    func addCardViews(newCardViews: [CardView]) {
        cardViews += newCardViews
        newCardViews.forEach { (setCardView) in
            addSubview(setCardView)
        }
        layoutIfNeeded()
    }
    
    func removeCardViews(removedCardViews: [CardView]) {
        removedCardViews.forEach { (setCardView) in
            cardViews = cardViews.filter { $0 != setCardView }
            setCardView.removeFromSuperview()
        }
        layoutIfNeeded()
    }
    
    func resetCards() {
        cardViews.forEach { (cardView) in
            cardView.removeFromSuperview()
        }
        cardViews = []
        layoutIfNeeded()
    }
}
