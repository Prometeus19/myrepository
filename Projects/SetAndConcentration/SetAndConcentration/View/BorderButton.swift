//
//  BorderButton.swift
//  SetAndConcentration
//
//  Created by Prometei on 5/21/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func awakeFromNib() {
        layer.cornerRadius = 5
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        layer.borderWidth = 1
        layer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
    }
}
