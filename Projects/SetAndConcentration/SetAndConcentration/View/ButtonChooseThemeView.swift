//
//  ButtonChooseThemeView.swift
//  SetAndConcentration
//
//  Created by Prometei on 5/17/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class ButtonChooseThemeView: UIButton {
    
    func doButton(_ space: Int, _ title: String, _ widthButton: Int) -> UIButton {
        let button = UIButton(frame: CGRect(x: 0,
                                            y: space,
                                            width: widthButton,
                                            height: 40))
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont(name: "System", size: 30)
        button.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        return button
    }
}
