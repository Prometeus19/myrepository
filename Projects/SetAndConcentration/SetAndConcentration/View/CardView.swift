//
//  CardView.swift
//  Set
//
//  Created by Prometei on 5/11/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class CardView: UIView, NSCopying {
    
    lazy var constantForDraw = Constants.Draw()
    
    var maxFaceFrame: CGRect {
        return bounds.zoomed(by: constantForDraw.maxFaceSizeToBoundsSize)
    }
    
    var faceFrame: CGRect {
        let faceWidth = maxFaceFrame.height * constantForDraw.faceFrame
        return maxFaceFrame.insetBy(dx: (maxFaceFrame.width - faceWidth) / 2, dy: 0)
    }
    
    var backFrame: CGRect {
        let maxBackFrame = bounds
        let faceWidth = maxFaceFrame.height * 0.85
        return maxBackFrame.insetBy(dx: maxFaceFrame.width - faceWidth, dy: 0)
    }
    
    var pipHeight: CGFloat {
        return faceFrame.height * constantForDraw.pipHeightToFaceHeight
    }
    
    var interPipHeight: CGFloat {
        return (faceFrame.height - (3 * pipHeight)) / 2
    }
    
    var cornerRadius: CGFloat {
        return bounds.size.height * constantForDraw.cornerRadiusToBoundsHeight
    }
    
    var degree = CGFloat(Double.pi / 180)
    var cardBackgroundColor = UIColor.white { didSet { setNeedsDisplay() } }
    
    var isSelected = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var isFaceUp = false {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var numberOfFigures = 1 {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var color = UIColor.red {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var filling = Filling.solid {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    var figure = Figures.squiggle {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        cardBackgroundColor.setFill()
        roundedRect.fill()
        
        guard !isFaceUp else {
            drawCard()
            return
        }
        guard let cardBackImage = UIImage(named: "card-backwards",
                                          in: Bundle(for: self.classForCoder),
                                          compatibleWith: traitCollection) else { return }
        cardBackImage.draw(in: backFrame)
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        doButtonState()
    }
    
    func drawCard() {
        color.setFill()
        color.setStroke()
        
        switch numberOfFigures {
        case 1:
            _ = setRectForOneShape(by: faceFrame.midY - pipHeight / 2)
        case 2:
            let firstRect = setRectForOneShape(by: faceFrame.midY - interPipHeight / 2 - pipHeight)
            _ = setRectForSecondAndThirdShape(with: firstRect)
        case 3:
            let firstRect = setRectForOneShape(by: faceFrame.minY)
            let secondRect = setRectForSecondAndThirdShape(with: firstRect)
            _ = setRectForSecondAndThirdShape(with: secondRect)
        default:
            break
        }
    }
    
    func setRectForOneShape(by sqdy: CGFloat) -> CGRect {
        let origin = CGPoint(x: faceFrame.minX, y: sqdy)
        let size = CGSize(width: faceFrame.width, height: pipHeight)
        let firstRect = CGRect(origin: origin, size: size)
        drawShape(in: firstRect)
        return firstRect
    }
    
    func setRectForSecondAndThirdShape(with rect: CGRect) -> CGRect {
        let nextRect = rect.offsetBy(dx: 0, dy: pipHeight + interPipHeight)
        drawShape(in: nextRect)
        return nextRect
    }
    
    func drawShape(in rect: CGRect) {
        var path = UIBezierPath()
        
        switch figure {
        case .diamond:  path = drawDiamond(in: rect)
        case .squiggle: path = drawSquiggle(in: rect)
        case .oval:     path = drawOval(in: rect)
        }
        
        path.lineWidth = 3.0
        path.stroke()
        
        switch filling {
        case .solid:   path.fill()
        case .striped: doStripeShape(path: path, in: rect)
        default:       break
        }
    }
    
    func doStripeShape(path: UIBezierPath, in rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        path.addClip()
        doStripeRect(rect)
        context?.restoreGState()
    }
    
    func drawSquiggle(in rect: CGRect) -> UIBezierPath {
        let topSquiggle = UIBezierPath()
        let sqdx = rect.width * 0.1
        let sqdy = rect.height * 0.2
        topSquiggle.move(to: CGPoint(x: rect.minX,
                                     y: rect.midY))
        topSquiggle.addCurve(to: CGPoint(x: rect.minX + rect.width / 2,
                                         y: rect.minY + rect.height / 6),
                             controlPoint1: CGPoint(x: rect.width * 1 / 4,
                                                    y: rect.minY),
                             controlPoint2: CGPoint(x: rect.width / 2,
                                                    y: rect.minY + rect.height / 8 - sqdy))
        topSquiggle.addCurve(to: CGPoint(x: rect.minX + rect.width * 4 / 5,
                                         y: rect.minY + rect.height / 8),
                             controlPoint1: CGPoint(x: rect.minX + rect.width * 1 / 2 + sqdx,
                                                    y: rect.minY + rect.height / 8 + sqdy),
                             controlPoint2: CGPoint(x: rect.minX + rect.width * 4 / 5 - sqdx,
                                                    y: rect.minY + rect.height / 8 + sqdy))
        
        topSquiggle.addCurve(to: CGPoint(x: rect.minX + rect.width,
                                         y: rect.minY + rect.height / 2),
                             controlPoint1: CGPoint(x: rect.minX + rect.width * 4 / 5 + sqdx,
                                                    y: rect.minY + rect.height / 8 - sqdy),
                             controlPoint2: CGPoint(x: rect.minX + rect.width,
                                                    y: rect.minY))
        
        let lowerSquiggle = UIBezierPath(cgPath: topSquiggle.cgPath)
        lowerSquiggle.apply(CGAffineTransform.identity.rotated(by: CGFloat.pi))
        lowerSquiggle.apply(CGAffineTransform.identity.translatedBy(x: bounds.width, y: bounds.height - 0.2))
        topSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        topSquiggle.append(lowerSquiggle)
        return topSquiggle
    }
    
    func drawOval(in rect: CGRect) -> UIBezierPath {
        let oval = UIBezierPath()
        let radius = rect.height / 2
        oval.addArc(withCenter: CGPoint(x: rect.minX + radius, y: rect.minY + radius),
                    radius: radius, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi * 3 / 2, clockwise: true)
        oval.addLine(to: CGPoint(x: rect.maxX - radius, y: rect.minY))
        oval.addArc(withCenter: CGPoint(x: rect.maxX - radius, y: rect.maxY - radius),
                    radius: radius, startAngle: CGFloat.pi * 3 / 2, endAngle: CGFloat.pi / 2, clockwise: true)
        oval.close()
        return oval
    }
    
    func drawDiamond(in rect: CGRect) -> UIBezierPath {
        let diamond = UIBezierPath()
        diamond.move(to: CGPoint(x: rect.minX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        diamond.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        diamond.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        diamond.close()
        return diamond
    }
    
    func doStripeRect(_ rect: CGRect) {
        let stripe = UIBezierPath()
        stripe.lineWidth = 1.0
        stripe.move(to: CGPoint(x: rect.minX, y: bounds.minY))
        stripe.addLine(to: CGPoint(x: rect.minX, y: bounds.maxY))
        let stripeCount = Int(faceFrame.width / constantForDraw.interStripeSpace)
        for _ in 1...stripeCount {
            let translation = CGAffineTransform(translationX: constantForDraw.interStripeSpace, y: 0)
            stripe.apply(translation)
            stripe.stroke()
        }
    }
    
    func doButtonState() {
        backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        isOpaque = false
        contentMode = .redraw
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = constantForDraw.borderWidth
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        layer.borderColor = isSelected ? #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1).cgColor : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1).cgColor
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = CardView()
        copy.numberOfFigures = numberOfFigures
        copy.filling = filling
        copy.color = color
        copy.figure = figure
        copy.isSelected =  false
        copy.isFaceUp = true
        copy.bounds = bounds
        copy.frame = frame
        copy.alpha = 1
        return copy
    }
    
    // MARK: Animation
    
    func doDealAnimation(from deckCenter: CGPoint, delay: TimeInterval) {
        let currentCenter = center
        let currentBounds = bounds
        
        alpha = 1
        center =  deckCenter
        bounds = CGRect(x: 0,
                        y: 0,
                        width: 0.6 * bounds.width,
                        height: 0.6 * bounds.height)
        isFaceUp = false
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 1,
            delay: delay,
            options: [],
            animations: {
                self.center = currentCenter
                self.bounds = currentBounds
                UIView.transition(with: self,
                                  duration: 0.5,
                                  options: [],
                                  animations: {
                                    self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                })
        },
            completion: { _ in
                UIView.transition(
                    with: self,
                    duration: 0.3,
                    options: [.transitionFlipFromLeft],
                    animations: {
                    self.isFaceUp = true
                })
        })
    }
}
