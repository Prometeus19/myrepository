//
//  ConcentrationThemeChooserViewController.swift
//  ConcentrationCopy
//
//  Created by Prometei on 5/16/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

// MARK: Ded style

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {
    
    lazy var button = ButtonChooseThemeView()
    lazy var themes = Themes()
    lazy var constant = Constants.Draw()
    
    @IBOutlet weak var buttonStackView: UIStackView!
    
    var splitViewDetailViewController: ConcentrationViewController? {
        return splitViewController?.viewControllers.last as? ConcentrationViewController
    }
    
    var lastSeguedToConcentrationViewController: ConcentrationViewController?
    
    // My function to create buttons in buttonStackView for choosing themes
    override func viewDidLoad() {
        var spaceBetweenButtons = 0
        let widthButton = Int(buttonStackView.frame.width)
        for theme in themes.themesArray {
            let oneButton = button.doButton(spaceBetweenButtons, theme.key, widthButton)
            oneButton.addTarget(self, action: #selector(chooseTheme), for: .touchUpInside)
            spaceBetweenButtons += constant.spaceBetweenButtons
            buttonStackView.addSubview(oneButton)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        splitViewController?.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
              identifier == "Choose Theme",
              let cvc = segue.destination as? ConcentrationViewController else { return }
        guard let themeName = (sender as? UIButton)?.currentTitle,
              let theme = themes.themesArray[themeName] else { return }
        cvc.theme = theme
        lastSeguedToConcentrationViewController = cvc
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool {
        guard let cvc = secondaryViewController as? ConcentrationViewController else { return false }
        return cvc.theme == nil
    }
    
    @objc func chooseTheme(_ sender: UIButton) {
        if let cvc = splitViewDetailViewController {
            guard let themeName = sender.currentTitle,
                  let theme = themes.themesArray[themeName] else { return }
            cvc.theme = theme
        } else if let cvc = lastSeguedToConcentrationViewController {
            guard let themeName = sender.currentTitle,
                  let theme = themes.themesArray[themeName] else { return }
            cvc.theme = theme
            navigationController?.pushViewController(cvc, animated: true)
        } else {
            performSegue(withIdentifier: "Choose Theme", sender: sender)
        }
    }
}
