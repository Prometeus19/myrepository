//
//  ViewController.swift
//  Concentration
//
//  Created by Prometei on 4/19/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

// MARK: Old Project - "Concentration"

import UIKit

class ConcentrationViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    
    var theme: [String]? {
        didSet {
            emojiChoises = theme ?? [""]
            emoji = [:]
            updateViewFromModel()
        }
    }

    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!

    var emojiChoises = ["🦇", "😱", "🙀", "😈", "🎃", "👻", "🍭", "🍬", "🍎"]
    var emoji: [Int: String] = [:]

    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else { return }
        UIView.transition(with: cardButtons[cardNumber],
                          duration: 0.6,
                          options: [.transitionFlipFromLeft],
                          animations: {
                              self.game.chooseCard(at: cardNumber)
        })
        updateLabels("Flips: \(game.countFlipCard)", "Score: \(game.countScore)")
        updateViewFromModel()
    }
    
    @IBAction func startNewGame(_ sender: UIButton) {
        emoji.removeAll()
        emojiChoises = theme ?? ["🦇", "😱", "🙀", "😈", "🎃", "👻", "🍭", "🍬", "🍎"]
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        updateLabels("Flips: 0", "Score: 0")
        updateViewFromModel()
    }

    func updateViewFromModel() {
        guard cardButtons != nil else { return }
            for index in cardButtons.indices {
                let button = cardButtons[index]
                let card = game.cards[index]
                if card.isFaceUp {
                    button.setTitle(emoji(for: card), for: .normal)
                    button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                } else {
                    button.setTitle("", for: .normal)
                    button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 0) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    button.isEnabled = button.backgroundColor == #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 0) ? false : true
                }
            }
    }
    
    func emoji(for card: ConcentrationCard) -> String {
        if emoji[card.identifier] == nil, emojiChoises.count > 0 {
            let randomIndex = Int.random(in: 1..<emojiChoises.count)
            emoji[card.identifier] = emojiChoises.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
    
    func updateLabels(_ firstTitleLabel: String, _ secondTitleLabel: String) {
        flipCountLabel.text = firstTitleLabel
        scoreLabel.text = secondTitleLabel
    }
}
