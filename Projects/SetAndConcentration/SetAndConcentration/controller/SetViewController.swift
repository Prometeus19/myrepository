//
//  ViewController.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class SetViewController: UIViewController, UIDynamicAnimatorDelegate {
    
    // MARK: Properties
    
    lazy var game = Set()
    lazy var animator: UIDynamicAnimator = {
        let animator = UIDynamicAnimator(referenceView: self.boardView)
        animator.delegate = self
        return animator
    }()
    
    lazy var cardViewBehavior = CardViewBehavior(in: animator)
    
    var dealCardViews: [CardView] {
        return  boardView.cardViews.filter { $0.alpha == 0 }
    }
    
    var cgPointButton: CGPoint {
        return view.convert(buttonStackView.center, to: self.view)
    }
    
    var matchedCardArray: [CardView] {
        guard game.stateCards == .set else { return [] }
        let indexesArray = game.indexesArray
        var matchedSetArray = [CardView]()
        for index in indexesArray {
            for indexInCardViews in 0..<boardView.cardViews.count where indexInCardViews == index {
                matchedSetArray.append(boardView.cardViews[indexInCardViews])
            }
        }
        return matchedSetArray
    }

    var count = 0
    var removableCardsArray = [CardView]()
    
    @IBOutlet weak var boardView: BoardView! {
        didSet {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dealThreeCards(_:)))
            swipeDown.direction = .down
            boardView.addGestureRecognizer(swipeDown)
            
            let rotate = UIRotationGestureRecognizer(target: self, action: #selector(shuffleAllCardsOnTable))
            boardView.addGestureRecognizer(rotate)
        }
    }
    
    @IBOutlet weak var setLabel: UILabel!
    @IBOutlet weak var dealThreeCardsButton: UIButton!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    // MARK: Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabel.text = ""
        game.createArrayCardsForGame()
        updateViewFromModel()
    }
    
    // MARK: Actions
    
    @IBAction func startNewGame(_ sender: UIButton) {
        SetCard.identifierFactory = 0
        game = Set()
        dealThreeCardsButton.isEnabled = true
        boardView.resetCards()
        removableCardsArray.forEach {(removableCard) in removableCard.removeFromSuperview()}
        removableCardsArray = []
        viewDidLoad()
        updateViewFromModel()
    }
    
    @IBAction func dealThreeCards(_ sender: UIButton) {
        if game.cards.count > 0 {
            game.dealThreeCards()
            updateViewFromModel()
        } else {
            dealThreeCardsButton.isEnabled = false
        }
    }
    
    @IBAction func searchSetOnTable(_ sender: UIButton) {
        if !game.searchSetOnTable() && game.cards.count == 0 {
            showAlert("End of the game!", "There are no more sets on the table! YOU WON!:) Start new game!")
        } else if !game.searchSetOnTable() && game.cards.count > 0 {
            showAlert("No sets!", "There are no sets on the table! Deal three more cards!")
        } else if game.searchSetOnTable() {
            updateViewFromModel()
        }
    }
    
    // MARK: Gestures
    
    func addTapGestureRecognizer(for cardView: CardView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCard(recognizedBy: )))
        cardView.addGestureRecognizer(tap)
    }
    
    @objc func tapOnCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard case .ended = recognizer.state else { return }
        guard let cardView = recognizer.view as? CardView,
              let index = boardView.cardViews.firstIndex(of: cardView) else { return }
        game.chooseCard(at: index)
        updateViewFromModel()
    }
    
    @objc func shuffleAllCardsOnTable() {
        game.shuffleCardsInGame()
        updateViewFromModel()
    }
    
    // MARK: Update views
    
    func updateViewFromModel() {
        updateCardViewsFromModel()
        switch game.stateCards {
        case .set:      setLabel.text = "SET!"
        case .notSet:   setLabel.text = "NOT SET!"
        case .nothing:  break
        }
    }
    
    func updateCardViewsFromModel() {
        if boardView.cardViews.count - game.cardsInGame.count > 0 {
            boardView.removeCardViews(removedCardViews: matchedCardArray)
            return
        }
        
        var newCardViews = [CardView]()
        let numberCardViews = boardView.cardViews.count
        
        for index in game.cardsInGame.indices {
            let card = game.cardsInGame[index]
            if index > (numberCardViews - 1) {
                let cardView = CardView()
                updateCardView(cardView, for: card)
                cardView.alpha = 0
                addTapGestureRecognizer(for: cardView)
                newCardViews += [cardView]
            } else {
                updateOldCardsOnBoard(card, at: index)
            }
        }
        
        boardView.addCardViews(newCardViews: newCardViews)
        
        if matchedCardArray.count == 3 {
            doFlyAwayAnimation()
        }
        
        if dealCardViews.count > 0 {
            doDealAnimation()
        }
    }
    
    func updateOldCardsOnBoard( _ card: SetCard, at index: Int) {
        let cardView = boardView.cardViews[index]
        if cardView.alpha < 1 &&
            cardView.alpha > 0 &&
            game.stateCards == .notSet {
            cardView.alpha = 0
        }
        if matchedCardArray.count < 3 {
            updateCardView(cardView, for: card)
        }
    }
    
    func updateCardView(_ cardView: CardView, for card: SetCard) {
        guard let figure = card.figure, let filling = card.filling else { return }
        cardView.figure = figure
        cardView.filling = filling
        cardView.color = card.color
        cardView.numberOfFigures = card.numberOfFigures
        cardView.isSelected = card.isSelected
    }
    
    // MARK: Animation
    
    func doDealAnimation() {
        var currentDealCard = 0
        
        let timeInterval =  0.15  * Double(boardView.rowsGrid + 1)
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { (_) in
            for cardView in self.dealCardViews {
                cardView.doDealAnimation(from: self.cgPointButton,
                                         delay: TimeInterval(currentDealCard) * 0.25)
                currentDealCard += 1
            }
        }
    }
    
    func doFlyAwayAnimation() {
        let countCardsReadyForFly = matchedCardArray.filter {($0.alpha < 1 && $0.alpha > 0)}.count
        guard game.stateCards == .set, countCardsReadyForFly == 0 else { return }
        matchedCardArray.forEach { setCardView in
            setCardView.alpha = 0.2
            guard let copyCard = setCardView.copy() as? CardView else { return }
            removableCardsArray.append(copyCard)
        }
        
        removableCardsArray.forEach { removableCard in
            boardView.addSubview(removableCard)
            cardViewBehavior.addItem(removableCard)
        }
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        removableCardsArray.forEach { (removableCard) in
            UIView.transition(
                with: removableCard,
                duration: 0.5,
                options: [.transitionFlipFromLeft],
                animations: {
                    removableCard.isFaceUp = false
                    removableCard.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi / 2.0)
                    removableCard.bounds = CGRect(x: 0,
                                                  y: 0,
                                                  width: 0.6 * removableCard.bounds.width,
                                                  height: 0.6 * removableCard.bounds.height)
                    
            },
                completion: { (_) in
                    self.cardViewBehavior.removeItem(removableCard)
                    removableCard.removeFromSuperview()
                    self.removableCardsArray = self.removableCardsArray.filter { $0 != removableCard }
            })
        }
    }
    
    // MARK: Alerts
    
    func showAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
