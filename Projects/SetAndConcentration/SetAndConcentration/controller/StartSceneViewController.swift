//
//  StartSceneViewController.swift
//  SetAndConcentration
//
//  Created by Prometei on 5/17/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class StartSceneViewController: UIViewController {
    
    // MARK: Properties
    
    lazy var constant = Constants.Animation()

    @IBOutlet weak var startSceneImageView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imageAndLabelView: UIView!
    @IBOutlet weak var dedokProductionLabel: UILabel!

    var degree = CGFloat(Double.pi / 180)
    
    // MARK: Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.doAnimationLabelAndProgressView(#selector(self.updateProgressView))
            self.progressView.setProgress(0, animated: true)
            self.startSceneImageView.image = UIImage(named: "grandfather")
            self.doAnimationLabelAndProgressView(#selector(self.rotateImage))
        })

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(6), execute: {
            self.startSceneImageView.removeFromSuperview()
            self.progressView.removeFromSuperview()
            self.dedokProductionLabel.text = "Dedok Production"
            UIView.animate(withDuration: 1,
                           delay: 0,
                           options: [],
                           animations: {
                            self.dedokProductionLabel.frame = CGRect(
                                x: self.imageAndLabelView.frame.midX,
                                y: self.imageAndLabelView.frame.minY,
                                width: self.imageAndLabelView.frame.width,
                                height: self.imageAndLabelView.frame.height / 6)
            }, completion: nil)
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(8), execute: {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            guard let tabBarViewController = mainStoryboard.instantiateViewController(withIdentifier: "Go in game")
                as? UITabBarController else { return }
            self.present(tabBarViewController, animated: true, completion: nil)
        })
    }
    
    // MARK: Animation "selectors"
    
    @objc func rotateImage() {
        guard startSceneImageView != nil else { return }
        UIView.animate(withDuration: constant.startingControllerTimeInterval,
                       delay: 0.3,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: {
                        self.startSceneImageView.transform = CGAffineTransform(rotationAngle: self.degree)
        })
        self.degree += CGFloat(Double.pi / 180)
    }
    
    @objc func updateProgressView() {
        guard progressView != nil else { return }
        guard progressView.progress != 1 else { return }
        self.progressView.progress += 1 / 600
    }
    
    // MARK: Animation
    
    func doAnimationLabelAndProgressView(_ selector: Selector) {
        Timer.scheduledTimer(timeInterval: constant.startingControllerTimeInterval,
                             target: self,
                             selector: selector,
                             userInfo: nil,
                             repeats: true)
    }
}
