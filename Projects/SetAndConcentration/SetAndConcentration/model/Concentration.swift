//
//  Concentration.swift
//  Concentration
//
//  Created by Prometei on 4/23/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

class Concentration {
    
    var cards: [ConcentrationCard] = []
    var indexOfOneAndOnlyFaceUpCard: Int?
    var countFlipCard = 0
    var countScore = 0
    
    init(numberOfPairsOfCards: Int) {
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        countFlipCard += 1
        
        guard !cards[index].isMatched else { return }
        
        guard let matchIndex = indexOfOneAndOnlyFaceUpCard,
              matchIndex != index else {
            flipdownOneCard(index)
            return
        }
        compareIndexes(matchIndex, and: index)
    }
    
    func flipdownOneCard(_ index: Int) {
        for flipdownIndex in cards.indices {
            cards[flipdownIndex].isFaceUp = false
        }
        cards[index].isFaceUp = true
        indexOfOneAndOnlyFaceUpCard = index
    }
    
    func compareIndexes(_ matchIndex: Int, and index: Int) {
        if cards[matchIndex].identifier == cards[index].identifier {
            cards[matchIndex].isMatched = true
            cards[index].isMatched = true
            countScore += 2
        } else {
            countScore -= 1
        }
        cards[index].isFaceUp = true
        indexOfOneAndOnlyFaceUpCard = nil
    }
}
