//
//  Card.swift
//  Concentration
//
//  Created by Prometei on 4/23/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

struct ConcentrationCard {
    
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        ConcentrationCard.identifierFactory += 1
        return ConcentrationCard.identifierFactory
    }
}
