//
//  Set.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import Foundation

enum StatesOfSelectedCards {
    case set, notSet, nothing
}

enum StatesOfButtonSearchSet {
    case stateOn, stateOff
}

class Set {
    
    lazy var deck = SetDeck()
    
    var cards = [SetCard]()
    var cardsInGame = [SetCard]()
    var cardsInSet = [SetCard]()
    var checkLoop = 0
    var identifierArray = [Int]()
    var indexesArray = [Int]()
    var stateCards = StatesOfSelectedCards.nothing
    var stateSearchButton = StatesOfButtonSearchSet.stateOff
    
    init() {
        cards = deck.fillDeck()
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        if cardsInGame[index].isSelected {
            clearIndexesArrayAndCardsInSetArray(at: index)
        } else if cardsInSet.count < 3 && !cardsInGame[index].isSelected {
            addCardsInSeparateArrays(at: index)
            if (cardsInSet.count == 3 && cards.count > 0) ||
               (cardsInSet.count == 3 && cards.count == 0) {
                stateSearchButton = .stateOn
                checkSet()
            }
        } else if !cardsInGame[index].isSelected && cardsInSet.count == 3 {
            checkSet()
            guard case .stateOff = stateSearchButton else { return }
            addCardsInSeparateArrays(at: index)
            stateSearchButton = .stateOff
        }
    }
    
    func addCardsInSeparateArrays(at index: Int) {
        cardsInSet.append(cardsInGame[index])
        identifierArray.append(cardsInGame[index].identifier)
        if indexesArray.count == 3 {
            indexesArray.removeAll()
            stateCards = .notSet
        }
        indexesArray.append(index)
        cardsInGame[index].isSelected = true
    }
    
    func checkSet() {
        let isSet = checkOnSet(cardsInSet)
        stateCards = isSet ? .set : .notSet
        doSet()
        updateIdentifireArrayAndCardInSet()
    }
    
    func updateIdentifireArrayAndCardInSet() {
        for indexCard in cardsInGame.indices {
            cardsInGame[indexCard].isSelected = false
        }
        identifierArray.removeAll()
        cardsInSet.removeAll()
    }
    
    func checkOnSet(_ setArray: [SetCard]) -> Bool {
        let firstCard = setArray[0]
        let secondCard = setArray[1]
        let thirdCard = setArray[2]
        guard checkColor(firstCard, secondCard, thirdCard),
              checkFigure(firstCard, secondCard, thirdCard),
              checkNumberOfFigures(firstCard, secondCard, thirdCard),
              checkFillingCard(firstCard, secondCard, thirdCard) else {
                  return false
              }
        return true
    }
    
    func checkColor(_ firstCard: SetCard, _ secondCard: SetCard, _ thirdCard: SetCard) -> Bool {
        if firstCard.color == secondCard.color && firstCard.color == thirdCard.color {
            return true
        } else if firstCard.color != secondCard.color &&
                  firstCard.color != thirdCard.color &&
                  secondCard.color != thirdCard.color {
            return true
        }
        return false
    }
    
    func checkFigure(_ firstCard: SetCard, _ secondCard: SetCard, _ thirdCard: SetCard) -> Bool {
        if firstCard.figure == secondCard.figure && firstCard.figure == thirdCard.figure {
            return true
        } else if firstCard.figure != secondCard.figure &&
                  firstCard.figure != thirdCard.figure &&
                  secondCard.figure != thirdCard.figure {
            return true
        }
        return false
    }
    
    func checkNumberOfFigures(_ firstCard: SetCard, _ secondCard: SetCard, _ thirdCard: SetCard) -> Bool {
        if firstCard.numberOfFigures == secondCard.numberOfFigures &&
           firstCard.numberOfFigures == thirdCard.numberOfFigures {
            return true
        } else if firstCard.numberOfFigures != secondCard.numberOfFigures &&
                  firstCard.numberOfFigures != thirdCard.numberOfFigures &&
                  secondCard.numberOfFigures != thirdCard.numberOfFigures {
            return true
        }
        return false
    }
    
    func checkFillingCard(_ firstCard: SetCard, _ secondCard: SetCard, _ thirdCard: SetCard) -> Bool {
        if firstCard.filling == secondCard.filling && firstCard.filling == thirdCard.filling {
            return true
        } else if firstCard.filling != secondCard.filling &&
                  firstCard.filling != thirdCard.filling &&
                  secondCard.filling != thirdCard.filling {
            return true
        }
        return false
    }
    
    func doSet() {
        guard case .set = stateCards else { return }
        for identifier in identifierArray {
            for indexCardsInGame in 0..<cardsInGame.count {
                if identifier == cardsInGame[indexCardsInGame].identifier && cards.count > 0 {
                    cardsInGame[indexCardsInGame] = cards[0]
                    cards.removeFirst()
                } else if identifier == cardsInGame[indexCardsInGame].identifier &&
                          stateSearchButton == .stateOn &&
                          cards.count == 0 {
                    cardsInGame.remove(at: indexCardsInGame)
                    break
                }
            }
        }
    }
    
    func searchSetOnTable() -> Bool {
        identifierArray.removeAll()
        cardsInSet.removeAll()
        indexesArray.removeAll()
        if cardsInGame.count > 3 {
            for firstCard in 0..<cardsInGame.count {
                for secondCard in (firstCard+1)..<cardsInGame.count {
                    for thirdCard in (secondCard+1)..<cardsInGame.count {
                        let cardsForSearchSet = [cardsInGame[firstCard],
                                                 cardsInGame[secondCard],
                                                 cardsInGame[thirdCard]]
                        let isSet = checkOnSet(cardsForSearchSet)
                        if isSet {
                            identifierArray.removeAll()
                            indexesArray.removeAll()
                            cardsInSet.removeAll()
                            fillAllArraysForPlayGame(at: firstCard)
                            fillAllArraysForPlayGame(at: secondCard)
                            fillAllArraysForPlayGame(at: thirdCard)
                            stateSearchButton = .stateOn
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    func fillAllArraysForPlayGame(at index: Int) {
        cardsInGame[index].isSelected = true
        identifierArray.append(cardsInGame[index].identifier)
        cardsInSet.append(cardsInGame[index])
        indexesArray.append(index)
    }
    
    func createArrayCardsForGame() {
        for index in 0..<12 {
            cardsInGame.append(cards[index])
        }
        removeCardsFromDeck(12)
    }
    
    func dealThreeCards() {
        if cards.count > 0 {
            for index in 0..<3 {
                cardsInGame.append(cards[index])
            }
            removeCardsFromDeck(3)
        }
    }
    
    func removeCardsFromDeck(_ number: Int) {
        for _ in 0..<number {
            cards.removeFirst()
        }
    }
    
    func clearIndexesArrayAndCardsInSetArray(at index: Int) {
        cardsInGame[index].isSelected = false
        for indexCardsInSet in 0..<cardsInSet.count where
            cardsInGame[index].identifier == cardsInSet[indexCardsInSet].identifier {
                checkLoop = indexCardsInSet
                identifierArray.remove(at: indexCardsInSet)
                indexesArray = indexesArray.filter { $0 != index }
        }
        cardsInSet.remove(at: checkLoop)
    }
    
    func shuffleCardsInGame() {
        cardsInGame.shuffle()
    }
}
