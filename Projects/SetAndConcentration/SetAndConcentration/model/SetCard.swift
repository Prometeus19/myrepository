//
//  Card.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

enum Figures {
    case squiggle, diamond, oval
}

enum Filling {
    case solid, striped, unfilled
}

struct SetCard {
    
    var figure: Figures?
    var numberOfFigures = 0
    var color = UIColor.white
    var filling: Filling?
    var isSelected = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    init() {
        self.identifier = SetCard.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        SetCard.identifierFactory += 1
        return SetCard.identifierFactory
    }
}
