//
//  Deck.swift
//  Set
//
//  Created by Prometei on 4/25/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

struct SetDeck {
    
    lazy var deck = [SetCard]()
    lazy var constantForDeck = Constants.Deck()
    
    let colorArray = [UIColor.red, UIColor.green, UIColor.purple]
    let fillingArray: [Filling] = [.solid, .striped, .unfilled]
    let figureArray: [Figures] = [.squiggle, .diamond, .oval]
    var indexFillingArray = 0
    var indexFigureArray = 0
    var indexColorArray = 0
    var numberOfFigure = 0
    
    mutating func fillDeck() -> [SetCard] {
        for _ in 0..<3 {
            for indexForOneColorFigure in 0..<constantForDeck.allColorRange {
                if indexForOneColorFigure < constantForDeck.endIndexOutlineRange {
                    indexFillingArray = 0
                } else if indexForOneColorFigure >= constantForDeck.endIndexOutlineRange &&
                    indexForOneColorFigure < constantForDeck.endIntexStripedRange {
                    indexFillingArray = 1
                } else if indexForOneColorFigure >= constantForDeck.endIntexStripedRange &&
                    indexForOneColorFigure < constantForDeck.endIndexOfFilledRange {
                    indexFillingArray = 2
                }
                
                if indexForOneColorFigure % 3 == 0 {
                    numberOfFigure += 1
                }
                
                if indexFigureArray > 2 {
                    indexFigureArray = 0
                }
                
                if numberOfFigure > constantForDeck.maxNumberOfFigures {
                    numberOfFigure = 1
                }
                
                fillOneCard(figureArray[indexFigureArray],
                            numberOfFigure,
                            colorArray[indexColorArray],
                            fillingArray[indexFillingArray])
                
                indexFigureArray += 1
            }
            indexColorArray += 1
        }
        return deck
    }
    
    mutating func fillOneCard(_ figure: Figures, _ numberOfFigures: Int, _ color: UIColor, _ filling: Filling) {
        var card = SetCard()
        card.figure = figure
        card.numberOfFigures = numberOfFigures
        card.color = color
        card.filling = filling
        deck.append(card)
    }
}
