//
//  ViewController.swift
//  Task1
//
//  Created by Prometei on 4/9/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit

class NonProgrammViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = """
        I'm Zakharov Pavel
        Vladimirovich
        """
    }
}

