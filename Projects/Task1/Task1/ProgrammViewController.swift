//
//  ProgrammViewController.swift
//  Task1
//
//  Created by Prometei on 4/17/19.
//  Copyright © 2019 Prometei. All rights reserved.
//

import UIKit
import SnapKit

class ProgrammViewController: UIViewController {
    
    var profileImageContainer: UIImageView?
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 3
        label.textColor = .black
        label.textAlignment = .center
        label.text = """
        I'm Zakharov Pavel
        Vladimirovich
        """
        label.font = UIFont(name: "Helvetica-Bold", size: 22)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(nameLabel)
        
        let image: UIImage = UIImage(named: "IMG_2358.jpeg")!
        profileImageContainer = UIImageView(image: image)
        profileImageContainer?.clipsToBounds = true
        profileImageContainer?.layer.cornerRadius = 117

        if profileImageContainer != nil {
            self.view.addSubview(profileImageContainer!)
        } else {
            print("No photo!")
        }
        
        profileImageContainer?.snp.makeConstraints({ (make) in
            make.width.height.equalTo(234)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo((profileImageContainer?.snp.bottom)!).offset(30)
            make.centerX.equalToSuperview()
        }
    }
}
